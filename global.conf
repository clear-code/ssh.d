# 推奨設定集

#
# known_hostsはハッシュ化する(既定のyesのままにする)
#
# 解説: ホストへの接続を受け入れるとknown_hostsに記録されるが、
#       その際の通信先をハッシュ化して通信先を秘匿化する
#
# リスク: yesにしていないと、~/ssh配下がまるごと漏れた場合にknown_hostsを利用して
#        アクセスされてしまう可能性がある (.bash_historyを探すよりも楽に攻撃対象がわかる)
#
# 注意: HashKnownHostsについては、将来的に非推奨とされ別のオプションによって
#      置き換えられる(ObscureKnownHostnamesという候補あり)可能性があります。
# 参考: https://groups.google.com/g/opensshunixdev/c/e5-kTKpxcaI/m/bdVNyL4BBAAJ
#
HashKnownHosts yes

#
# 必須: パスワード認証で接続しにいかない
#
# 解説: サーバー側がパスワード認証を有効にしていた場合でも
#       クライアント側で明示的にパスワード認証での接続を禁止する
#
# リスク: yesだと、うっかり誤ったサーバーに接続した際にパスワードを漏洩する可能性がある
#
PasswordAuthentication no

#
# ホストキーを必ずチェックする
#
# 解説: ホストキーがknown_hostsに登録されていない場合は接続を拒否する。
#       事前にssh-keyscan  -H github.com >> ~/.ssh/known_hostsで登録するか、
#       十分に信頼できるネットワーク内ならばssh -o StrictHostKeyChecking=noで一時的に無効化して接続できる
#       頻繁にVMを作成・破棄する場合にはホストキーの登録の手間がかかる[1]
#
# リスク: noだと(DNS改ざんなど)意図しないサーバーに接続しにいってしまった場合であっても
#        fingerprintをきちんと確認せず受理してしまう可能性がある
#
# [1]のデメリットを考慮して、LAN内のみ信頼する場合には次のように特定のIPの範囲のみユーザーに確認をうながすのがよい
#
# Host 192.168.1.*
#    StrictHostKeyChecking ask
#
StrictHostKeyChecking yes

#
# ホストキーだけでなく、known_hostsのIPもチェックする
#
# 解説: StrictHostKeyCheckingとは独立。DNS spoofing対策として有効にする
#
# リスク: 
#
CheckHostIP yes

#
# 圧縮処理を有効にして帯域を節約する
#
# 解説: gitでテキストのコミットが多ければ効果が見込める。逆にバイナリをたくさんコミットするような場合には不向き
#
# リスク: 
#
Compression yes


#
# サーバーからの自動切断を防ぐ
#
# 解説: 通信がしばらく発生していない状態で接続を切断されてしまわないようにする。
#
# リスク: 
#
ServerAliveInterval 60

#
# StrictHostKeyCheckingの対象となるホスト鍵のfingerprintをあらかじめ指定しておく
#
# 解説: StrictHostKeyChecking=yesで接続できるようにサイトごとのfingerprintをあらかじめ準備します。
#
# GitHub:
#  curl --silent https://api.github.com/meta | jq  --raw-output ".ssh_keys[]" | sed 's/^/github.com /g' > known_hosts.github
# GitLab:
#  See https://docs.gitlab.com/ee/user/gitlab_com/#ssh-known_hosts-entries
#
# リスク: known_hosts.*を最新の状態に維持する必要があります。
#
#UserKnownHostsFile ~/.ssh/known_hosts ~/.ssh/known_hosts.github ~/.ssh/known_hosts.gitlab
