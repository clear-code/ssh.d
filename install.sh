#!/bin/sh

set -eux

clone_mode=${1:-https}

cd ~/

case $clone_mode in
  https)
    git clone https://gitlab.com/clear-code/ssh.d.git ssh.d
    ;;
  ssh)
    git clone git@gitlab.com:clear-code/ssh.d.git ssh.d
    ;;
  *)
    set +x
    echo "$clone_mode is not valid clone mode. Please specify 'https' (default) or 'ssh'."
    exit -1
    ;;
esac

if [ -d .ssh ]
then
  mv .ssh/config ssh.d/conf.d/my.conf
  mv .ssh/* ssh.d/
  rm -r .ssh
fi
mv ssh.d .ssh
